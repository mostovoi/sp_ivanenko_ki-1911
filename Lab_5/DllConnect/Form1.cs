﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Windows.Forms;
using Interface;
using System.Globalization;

namespace DllConnect
{
    public partial class Form1 : Form
    {
        private string functionsDllPAth = $@"{Environment.CurrentDirectory}\Functions.dll";
        private Type functionType;
        private object functionClass;
        public Form1()
        {
            InitializeComponent();
            try
            {
                string message = Interface.Interface.SayGreetings();
                MessageBox.Show(message);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
                throw;
            }
        }

        private void buttonLoadDll_Click(object sender, EventArgs e)
        {
            var currentProcess = Process.GetCurrentProcess();
            var count = currentProcess.Modules.Cast<ProcessModule>().Count(module => module.FileName == functionsDllPAth);
            
            if (count != 0) return;

            Assembly functionsDll;
            try
            {
                 functionsDll = Assembly.LoadFile(functionsDllPAth);
                 MessageBox.Show("Functions.dll. loaded");
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.ToString());
                throw;
            }

            functionType = functionsDll.GetType("Functions.Functions");
            functionClass = Activator.CreateInstance(functionType);
        }

        private void buttonStartMultiThreading_Click(object sender, EventArgs e)
        {
            if (functionClass != null)
            {
                    functionType.InvokeMember("ThreadTest", BindingFlags.InvokeMethod, Type.DefaultBinder, functionClass, new object[] { });
            }
            else
            {
                MessageBox.Show("Need to load Dll");
            }
        }

        private void buttonCheckGlobalization_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"Math test: sin(50) = {Math.Sin(50)}");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}