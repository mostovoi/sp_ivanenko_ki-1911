﻿namespace DllConnect
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonLoadDll = new System.Windows.Forms.Button();
            this.buttonStartMultiThreading = new System.Windows.Forms.Button();
            this.buttonCheckGlobalization = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // buttonLoadDll
            // 
            this.buttonLoadDll.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonLoadDll.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoadDll.Location = new System.Drawing.Point(20, 14);
            this.buttonLoadDll.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonLoadDll.Name = "buttonLoadDll";
            this.buttonLoadDll.Size = new System.Drawing.Size(165, 42);
            this.buttonLoadDll.TabIndex = 0;
            this.buttonLoadDll.Text = "Load DLL";
            this.buttonLoadDll.UseVisualStyleBackColor = false;
            this.buttonLoadDll.Click += new System.EventHandler(this.buttonLoadDll_Click);
            // 
            // buttonStartMultiThreading
            // 
            this.buttonStartMultiThreading.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonStartMultiThreading.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartMultiThreading.Location = new System.Drawing.Point(23, 120);
            this.buttonStartMultiThreading.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonStartMultiThreading.Name = "buttonStartMultiThreading";
            this.buttonStartMultiThreading.Size = new System.Drawing.Size(162, 47);
            this.buttonStartMultiThreading.TabIndex = 1;
            this.buttonStartMultiThreading.Text = "Multithreading sort";
            this.buttonStartMultiThreading.UseVisualStyleBackColor = false;
            this.buttonStartMultiThreading.Click += new System.EventHandler(this.buttonStartMultiThreading_Click);
            // 
            // buttonCheckGlobalization
            // 
            this.buttonCheckGlobalization.BackColor = System.Drawing.Color.Gainsboro;
            this.buttonCheckGlobalization.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCheckGlobalization.Location = new System.Drawing.Point(23, 64);
            this.buttonCheckGlobalization.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.buttonCheckGlobalization.Name = "buttonCheckGlobalization";
            this.buttonCheckGlobalization.Size = new System.Drawing.Size(162, 47);
            this.buttonCheckGlobalization.TabIndex = 2;
            this.buttonCheckGlobalization.Text = "Math Dll";
            this.buttonCheckGlobalization.UseVisualStyleBackColor = false;
            this.buttonCheckGlobalization.Click += new System.EventHandler(this.buttonCheckGlobalization_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(206, 181);
            this.Controls.Add(this.buttonCheckGlobalization);
            this.Controls.Add(this.buttonStartMultiThreading);
            this.Controls.Add(this.buttonLoadDll);
            this.Font = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(5, 4, 5, 4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ConnectDll";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        private System.Windows.Forms.Button buttonCheckGlobalization;
        private System.Windows.Forms.Button buttonLoadDll;
        private System.Windows.Forms.Button buttonStartMultiThreading;

        #endregion
    }
}