﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Windows.Forms;
using System.Xml;
using TaskScheduler;

namespace Lab04
{
    public partial class Form1 : Form
    {
        public Form1()
        {
          
            InitializeComponent();
            UpdateAllUsersPrograms();
            UpdateCurrentUserPrograms();
            UpdateAllUsersScheduledTasks();
            UpdateCurrentUserScheduledTasks();
           
        }

        private void UpdateAllUsersPrograms()
        {
            richTextBoxAllUserPrograms.Clear();
            var services = Registry.LocalMachine
                .OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run")
                .GetValueNames()
                .ToList();
            services.ForEach(s => richTextBoxAllUserPrograms.AppendText(s + Environment.NewLine));
        }

        private void UpdateCurrentUserPrograms()
        {
            richTextBoxCurrentUserPrograms.Clear();
            var services = Registry.CurrentUser
                .OpenSubKey(@"Software\Microsoft\Windows\CurrentVersion\Run")
                .GetValueNames()
                .Where(s => !string.IsNullOrEmpty(s))
                .ToList();
            services.ForEach(s => richTextBoxCurrentUserPrograms.AppendText(s + Environment.NewLine));
        }

        private void UpdateAllUsersScheduledTasks()
        {
            richTextBoxScheduler.Clear();
            ParseScheduleTasks().ToList().ForEach(s => richTextBoxScheduler.AppendText(s + Environment.NewLine));
        }

        private IList<string> ProcessTaskFolder(ITaskFolder taskFolder, string author = "")
        {
            var scheduledTasks = new List<string>();
            var taskCol = taskFolder.GetTasks((int)_TASK_ENUM_FLAGS.TASK_ENUM_HIDDEN);

            for (int idx = 1; idx <= taskCol.Count; idx++)
            {
                if (string.IsNullOrEmpty(author))
                    scheduledTasks.Add(taskCol[idx].Name);

                var principal = GetAuthorFromXmlString(taskCol[idx].Xml);
                if (!string.IsNullOrEmpty(principal) && principal == author)
                    scheduledTasks.Add(taskCol[idx].Name);
            }

            var taskFolderCol = taskFolder.GetFolders(0);
            for (int idx = 1; idx <= taskFolderCol.Count; idx++)
                ProcessTaskFolder(taskFolderCol[idx]);

            return scheduledTasks;
        }

        private string GetAuthorFromXmlString(string xmlString)
        {
            if (string.IsNullOrEmpty(xmlString))
                return string.Empty;

            var xml = new XmlDocument();
            xml.LoadXml(xmlString);
            var registrationInfo = xml.GetElementsByTagName("RegistrationInfo");
            var childNodes = registrationInfo[0].ChildNodes;
            if (registrationInfo.Count == 0 || childNodes.Count == 0)
                return string.Empty;

            var authorNode = string.Empty;
            foreach (XmlNode node in childNodes)
                if (node.Name == "Author")
                    authorNode = node.InnerText;

            return authorNode;
        }

        private IList<string> ParseScheduleTasks(bool forCurrentUser = false)
        {
            var taskService = new TaskScheduler.TaskScheduler();
            taskService.Connect();

            if (!forCurrentUser)
                return ProcessTaskFolder(taskService.GetFolder("\\"));

            var authorName = string.Join("\\", GetAuthorName(taskService));
            return ProcessTaskFolder(taskService.GetFolder("\\"), authorName);
        }

        private IEnumerable<string> GetAuthorName(TaskScheduler.TaskScheduler taskService)
        {
            if (taskService != null)
            {
                if (!string.IsNullOrEmpty(taskService.ConnectedDomain))
                    yield return taskService.ConnectedDomain;

                if (!string.IsNullOrEmpty(taskService.ConnectedUser))
                    yield return taskService.ConnectedUser;
            }
        }

        private void UpdateCurrentUserScheduledTasks()
        {
            richTextBoxCurrentUserTasks.Clear();
            ParseScheduleTasks(true).ToList().ForEach(s => richTextBoxCurrentUserTasks.AppendText(s + Environment.NewLine));
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void buttonRefresh_Click(object sender, EventArgs e)
        {
            
            UpdateAllUsersPrograms();
            UpdateCurrentUserPrograms();
            UpdateAllUsersScheduledTasks();
            UpdateCurrentUserScheduledTasks();
        }

        private void buttonAddToAutoStart_Click(object sender, EventArgs e)
        {
            var toSetAutorun = textBoxProgramToAutorun.Text.ToString();
            if (!string.IsNullOrEmpty(toSetAutorun))
                using (var key = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\Microsoft\Windows\CurrentVersion\Run", true))
                    key.SetValue(toSetAutorun, "\"" + toSetAutorun + "\"");
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void ExportKey(string key, string path)
        {
            string arguments = $"reg export \"{key}\" \"{path}\" /y";
            string strCmdText = "/C " + arguments;
            const string FILE_NAME = "CMD.exe";

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo()
                {
                    FileName = FILE_NAME,
                    Arguments = strCmdText,
                    UseShellExecute = false,
                    CreateNoWindow = false,
                }
            };

            try
            {
                process.Start();
                if (process != null)
                    process.WaitForExit();
            }
            finally { if (process != null) { process.Dispose(); } }
        }

        private void buttonCopy_Click_1(object sender, EventArgs e)
        {
            string key = textBoxSubKey.Text;
            var path = @"С:\Desktop\exported.reg";
            ExportKey(key, path);
            MessageBox.Show("Копіювання завершено!");

            textBoxSubKey.Text = string.Empty;
        }

        private void buttonAddToAssociation_Click(object sender, EventArgs e)
        {
            var path = $@"{Environment.CurrentDirectory}\txtAssociation.reg";
            string arguments = $"/s {path}";
            var regeditProcess = Process.Start($"regedit.exe", arguments);
            regeditProcess.WaitForExit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}